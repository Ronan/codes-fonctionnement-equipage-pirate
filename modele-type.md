**Équipage "NOM DE L'ÉQUIPAGE"**
---

**PARTIE 1 : Champ d'action**
---

Article 1
---

Il est organisé par les Pirates selon les articles 6 et suivants des statuts un équipage ayant pour dénomination : 
" NOM DE L'ÉQUIPAGE ".

Article 2
---

Cet équipage a le champ d'action suivant : thématique/événementiel/géographique '(supprimer les mentions inutiles)'.

Le champ d'action est décrit de la manière suivante :

Article 3
---

L'équipage peut présenter devant l'Assemblée permanente de manière annuelle ou ponctuelle une demande de fonds 
pour financer ses activités.

**PARTIE 2 : Durée**
---

Article 4
---

L'équipage est formé pour une durée d'un an renouvelable / pour une durée de X mois (obligatoirement inf. à 12 mois).

Article 5
---

Si durée d'un an renouvelable

L'équipage sera renouvelé après approbation par l'Assemblée permanente de son Rapport de fonctionnement 
détaillant ses activités durant l'année écoulée. 
Le Rapport de fonctionnement devra être déposé lors de la session du mois anniversaire de l'équipage.
(non modifiable sinon contraire aux statuts)

Si durée inf. à un an

L'équipage devra déposer un rapport de fonctionnement au terme de sa période d'activité.
Ce rapport devra être soumis à l'approbation de l'Assemblée permanente.
Non modifiable sinon contraire aux statuts

Article 6 : (espace réservé au Secrétariat)
---

L'équipage a été approuvé par l'Assemblée permanente de (mois et année).

**PARTIE 3 : Membres de l'équipage**
---

(Option 1) 

Article 7
---

L'équipage est accessible librement à l'ensemble des Pirates
sans distinction de compétences, de situation géographique ou tout autre critère restrictif.

Article 8
---

Les Pirates s'inscrivent librement sur la liste des membres de l'équipage via l'outil Personae

(Option 2)

Article 7
---

L'équipage est ouvert à tous mais les membres de l'équipage se réservent la possibilité
de réduire l'entrée selon les critères suivants :

- Nombre maximum de membres : (préciser)
- Compétence ou intérêt particulier en lien avec le champ d'action de l'équipage : (préciser)
- Rattachement géographique (autre qu'une obligation de domicile dans la zone déterminée par le champ d'action)
- autres ; préciser

Article 8
---

Les personnes qui procèdent à l'acceptation d'un membre dans un équipage sont :

    Le ou les capitaines
    Les membres de l'équipage

Article 8-1
---

Les personnes ayant été acceptées sont inscrites sur l'outil Personae par le Capitaine de l'équipage.

**PARTIE 4 : Capitaine**
---

Article 9
---

L'équipage est représenté par un (ou plus, précisez) capitaine qui est élu de la manière suivante : (choisir)

    Jugement majoritaire
    Scrutin majoritaire (1/2 tours)
    Scrutin de liste (uniquement si directement collégiale)

Article 10
---

Le mandat du Capitaine est de (préciser) an ou mois.

Article 11
---

Le capitaine peut être révoqué selon la procédure prévue par les statuts.

**PARTIE 5 : Autres membres**
---

Article 12
---

Le fonctionnement de l'équipage est également assuré par :

- (Préciser le titre et la fonction)
- (Préciser le titre et la fonction)
- Etc. 

NB : Le titre de porte-parole ne peut être accordé que par l'Assemblée permanente

Article 13
---

Ces autres membres sont élus de la façon suivante : (choisir)

    Jugement majoritaire
    Scrutin majoritaire (1/2 tours)
    Scrutin de liste (uniquement si directement collégiale)

(si distinction entre les postes préciser)

Article 14
---

Le mandat de ces autres membres est de (préciser) an ou mois. (si distinction entre les postes préciser)

**PARTIE 6 : Fréquence des réunions / Décisions**
---

Article 15
---

L'équipage se réunit au minimum (préciser) fois par an.

La fréquence des réunions est de (préciser) fois par mois/semaine/trimestre/an.

Article 16
---

Les réunions sont organisées de la manière suivante :

- Délai de convocation : (préciser)
- Plateforme de vote : Congressus 
- Quorum
- Majorité
- Publication du compte-rendu/relevé de décision/ autre : 
- Publication : Discourse 
- Archivage : Wiki

Autres...

À défaut, les règles sont les mêmes que celles en vigueur pour l'Assemblée permanente :  
- Quorum : 5% des membres de l'équipage  
- Majorité : Majorité absolue

Article 17
---

Les décisions prises par l'Équipage doivent respecter les Statuts et le Règlement intérieur.
Dans le cas contraire, les membres de l'équipage et ses représentants pourront faire l'objet de sanctions.
Non modifiable sinon contraire aux statuts

**PARTIE 7 : Modification**
---

Article 18
---

Toute modification de ce Code de fonctionnement devra être approuvée par l'Assemblée permanente.

Article 19
---

Les conditions de modification sont les suivantes :

    (Préciser ; délai de convocation + modalité de vote + quorum / autres)

**PARTIE 8 : Dissolution**
---

Article 20
---

L'équipage est automatiquement dissous : non modifiable sinon contraire aux statuts

- À l'échéance de la période d'activité prévue par le Code de fonctionnement
- Faute de présentation d'un rapport annuel à l'assemblée permanente
- Faute d'approbation du rapport annuel par l'assemblée permanente
- Après décision motivée du Conseil réglementaire et statutaire approuvée par l'assemblée permanente,
lorsque l'équipage a commis une faute grave ou violé les statuts et le règlement intérieur
de manière intentionnelle ou répétée.
Les membres de l'équipage peuvent faire l'objet de sanctions individuelles.

Article 21
---

Si durée d'1 an

L'équipage pourra être librement dissous par ses membres dans les conditions suivantes :

    (préciser ; prévoir un délai de convocation large + modalité de vote + quorum)

À défaut, les règles sont :   
- Délais de convocation : 15 jours
- Quorum : 10% des membres de l'équipage
-  Majorité : Majorité absolue

Si durée inf. 1 an

L'équipage pourra être librement dissous par ses membres par un vote pris à la majorité. 
Non modifiable sinon contraire aux statuts
- Le délai minimum de convocation est de (préciser) semaines.
- Le quorum est de (préciser)membres.
- Autres : (préciser si nécessaire)

À défaut, les règles sont  :
- Quorum : 10% des membres de l'équipage
- Délai de convocation : 15 jours

Article 22
---

Le capitaine de l'équipage informe le Conseil de Vie Interne de la dissolution dans un délai d'une semaine maximum
après la tenue du vote.
Non modifiable sinon contraire aux statuts

**PARTIE 9 : Contrôle**
---

Article 23
---

L'équipage a été fondé par : (préciser nom ou pseudo d'au minimum trois pirates fondateurs)

Article 24
---

Le présent Code de fonctionnement a été soumis au contrôle du Conseil réglementaire et statutaire
qui l'a déclaré conforme aux Statuts et au Règlement intérieur par une décision de (espace réservé au CRS) 