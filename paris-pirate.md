**Équipage "Paris Pirate"**
---

**PARTIE 1 : Champ d'action**
---

Article 1
---

Il est organisé par les Pirates selon les articles 6 et suivants des statuts un équipage ayant pour dénomination : 
"PARIS PIRATE".

Article 2
---

Le champ d'action de cet équipage s'étend de façon géographique sur la commune de Paris intra-muros.
Cet équipage aura comme activités l'animation du militantisme local, l'intégration de nouveaux adhérents parisiens, 
l'organisation et la participation à des évènements, la préparation d'élections locales.
Les membres de l'équipage s'engagent à respecter la courtoisie et la cordialité dans toutes les formes d’échanges.

Article 3
---

L'équipage peut présenter devant l'Assemblée permanente de manière annuelle ou ponctuelle une demande de fonds 
pour financer ses activités.

**PARTIE 2 : Durée**
---

Article 4
---

L'équipage est formé pour une durée d'un an renouvelable.

Article 5
---

L'équipage sera renouvelé après approbation par l'Assemblée permanente de son Rapport de fonctionnement 
détaillant ses activités durant l'année écoulée. 
Le Rapport de fonctionnement devra être déposé lors de la session du mois anniversaire de l'équipage.
(non modifiable sinon contraire aux statuts)

Article 6 : (espace réservé au Secrétariat)
---

L'équipage a été approuvé par l'Assemblée permanente de (mois et année).

**PARTIE 3 : Membres de l'équipage**
---

Article 7
---

L'équipage est ouvert à tous, mais les membres de l'équipage se réservent la possibilité 
de réduire l'entrée selon les critères suivants :

- Attachement aux activités du Parti Pirate sur Paris.
- Capacité à être présent régulièrement aux réunions physiques de l'équipage qui se tiendront sur Paris.

Article 8
---

- Les membres procèdent à l'acceptation d'un membre dans l'équipage par un vote à 66%.
- Un membre peut être radié de l'équipage suite à un vote à 66%.

Article 8-1
---

Les personnes ayant été acceptées sont inscrites sur l'outil Personae par le Capitaine de l'équipage.

**PARTIE 4 : Capitaine**
---

Article 9
---

L'équipage est représenté par un capitaine qui est élu au jugement majoritaire.

Article 10
---

Le mandat du Capitaine est de 1 an.

Les attributions du capitaine sont les suivantes :

- Organiser et présider une réunion au minimum une fois par an entre le renouvellement de l'équipage 
par l'assemblée permanente et le 1er octobre
- S’informer 1 fois tous les 2 mois des nouveaux adhérents habitants à Paris
- Contacter 1 fois tous les 2 mois les membres de l’équipage
- Organiser et présider une rencontre 1 fois tous les 2 mois
- Publier le CR des réunions et rencontres sur Discourse
- Archiver les CR des réunions et rencontres sur le wiki
- Poser la motion de renouvellement de l’équipage à sa date anniversaire
- Appeler et présider une réunion d’équipage après le renouvellement de l’équipage 
pour faire un bilan de l’année passée et désigner le prochain capitaine et le prochain second

Article 11
---

Le capitaine peut être révoqué selon la procédure prévue par les statuts.

**PARTIE 5 : Autres membres**
---

Article 12
---

Le fonctionnement de l'équipage est également assuré par le second.

Le capitaine a les 8 attributions décrites dans l'article 10.
S’il est manifeste pour le second qu'une action n'a pas été réalisée, il peut en prendre la charge.

Article 13
---

Le second est élu lors de l'élection du capitaine (Article 9).
Il s'agit du candidat arrivé 2e lors du scrutin.

Article 14
---
Le mandat de ces autres membres est de 1 an.

**PARTIE 6 : Fréquence des réunions / Décisions**
---

Article 15
---

L'équipage se réunit au minimum 5 fois par an. 

L'objectif est cependant de se réunir une fois tous les 2 mois.

Article 16
---

Les réunions sont organisées de la manière suivante :

- Délai de convocation : 15 jours
- Plateforme de vote : Congressus 
- Quorum : 5% des membres de l'équipage.
- Majorité : Majorité absolue
- Publication : Discourse
- Archivage : Wiki

Article 17
---

Les décisions prises par l'Équipage doivent respecter les Statuts et le Règlement intérieur.
Dans le cas contraire, les membres de l'équipage et ses représentants pourront faire l'objet de sanctions.
Non modifiable sinon contraire aux statuts

**PARTIE 7 : Modification**
---

Article 18
---

Toute modification de ce Code de fonctionnement devra être approuvée par l'Assemblée permanente.

Article 19
---

Les modifications peuvent être proposées au capitaine ou à son second d'une façon ou d'une autre 
pour qu'ils puissent les inscrire à l'ordre du jour d'une réunion.

**PARTIE 8 : Dissolution**
---

Article 20
---

L'équipage est automatiquement dissous : non modifiable sinon contraire aux statuts

- À l'échéance de la période d'activité prévue par le Code de fonctionnement
- Faute de présentation d'un rapport annuel à l'assemblée permanente
- Faute d'approbation du rapport annuel par l'assemblée permanente
- Après décision motivée du Conseil réglementaire et statutaire approuvée par l'assemblée permanente,
lorsque l'équipage a commis une faute grave ou violé les statuts et le règlement intérieur
de manière intentionnelle ou répétée.
Les membres de l'équipage peuvent faire l'objet de sanctions individuelles.

Article 21
---

L'équipage pourra être librement dissous par ses membres dans les conditions suivantes :

- Délais de convocation : 15 jours.
- Quorum : 10% des membres de l'équipage.
- Majorité : Majorité absolue

Article 22
---

Le capitaine de l'équipage informe le Conseil de Vie Interne de la dissolution dans un délai d'une semaine maximum
après la tenue du vote.
Non modifiable sinon contraire aux statuts

**PARTIE 9 : Contrôle**
---

Article 23
---

L'équipage a été fondé par :

- Ronan Le Roy
- Hélène Testud
- Romain David

Article 24
---

Le présent Code de fonctionnement a été soumis au contrôle du Conseil réglementaire et statutaire
qui l'a déclaré conforme aux Statuts et au Règlement intérieur par une décision de (espace réservé au CRS) 